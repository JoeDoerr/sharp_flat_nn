#!/bin/bash
#SBATCH --exclusive
#SBATCH --output=/home/joehdoerr/Desktop/ANN_activations_sharpness/sharp_flat_nn/slurm_jobs/slurm_MNIST.out
#SBATCH --time=03:00:00
#SBATCH --nodes=1
#SBATCH --job-name=slurm_MNIST.job
source activate tf24
srun python /home/joehdoerr/Desktop/ANN_activations_sharpness/sharp_flat_nn/opt_path_activations_sharp.py
