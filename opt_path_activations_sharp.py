#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Optimization pathway for testing small batch and large batch number of activations and sharpness each timestep on MNIST dataset

@author: Joe Doerr
"""

import sys
sys.path.append("../")

#other file includes
from run import number_of_activations_train, test_loss_and_sharpness, minimizer_calculation
from prop_layers import nn_model
from data_MNIST import train_in, train_label, test_in, test_label
import tensorflow as tf
from tflog import Make_summary_info
from simplified_run import make_dataset, test_zero_biases, sharpness, train_loss, train_loss_batched, fit_large_batch, full_loss

import os
currentdir = os.path.dirname(os.path.realpath(__file__)) #macro folder
parentdir = os.path.dirname(currentdir) #example folder

def main():
    
    shuffle_size=60000
    sb_size=600
    lb_size=6000
    lr=0.001
    sharp_iters=10
    num_batch_calc_loss=5 #20*5*600=60000=full_dataset_size
    activation_iters=100
    #$matlab saving
    sb_zb=[]
    lb_zb=[]
    sb_s= []
    lb_s= []
    sb_l= []
    lb_l= []

    #interesting idea could be checking zeros over averaging grads to see if any neurons are completely dead
    #from what I have seen its a very low number of dead neurons and it reduces dead neurons to even 0 as it trains
    #sb checking number of activations check each sb timestep
    sb_model = nn_model(3, 784, 100, 10) #nn_model(m_layers, m_input_size, m_hidden_size, m_output_size)
    log_path=currentdir+'/'+'logs_1'+'/sb'
    writer_sb = tf.summary.create_file_writer(log_path)
    #print("train_in_shape: ", train_in.shape) #the minibatch loop can sometimes include the whole dataset
    act_dataset=make_dataset(train_in, train_label, shuffle_size)
    sb_batched_dataset=act_dataset.batch(sb_size)
    act_dataset=act_dataset.batch(1)
    for ii in range(350): #multiple different timesteps to get the averaged number of increased neuron activations from
        print("small batch ", sb_size, "iteration: ", ii)
        
        sb_batched_dataset=sb_batched_dataset.shuffle(shuffle_size)
        print("dataset_shuffled")

        if ii % 2 == 0: #half the time check for activations, otherwise the bs of 1 takes so long
            act_dataset=act_dataset.shuffle(shuffle_size)
            zero_bias_vals = test_zero_biases(sb_model, act_dataset, activation_iters)
            sb_zb.append(zero_bias_vals)
            print("activations: ", zero_bias_vals)
            Make_summary_info({'zero_bias_vals':zero_bias_vals}, writer_sb, ii)
            print("activations: ", zero_bias_vals)

            sharp=sharpness(sb_model, sb_batched_dataset, sharp_iters, sb_size, num_batch_calc_loss)
            sb_s.append(sharp)
            print("sharpness: ", sharp)
            Make_summary_info({'sharpness':sharp}, writer_sb, ii)
            print("sharpness: ", sharp)
        else:
            loss = train_loss_batched(sb_model, sb_size, sb_batched_dataset, lr)
            sb_l.append(loss)
            print("loss: ", loss)
            Make_summary_info({'loss':loss}, writer_sb, ii)
            print("loss: ", loss)
        

    #lb checking number of activations check each lb timestep
    lb_model = nn_model(3, 784, 100, 10) #nn_model(m_layers, m_input_size, m_hidden_size, m_output_size)
    log_path=currentdir+'/'+'logs_1'+'/lb'
    lr=0.0003
    writer_lb = tf.summary.create_file_writer(log_path)
    act_dataset=make_dataset(train_in, train_label, shuffle_size)
    lb_batched_dataset=act_dataset.batch(sb_size) #sb so it can fit
    act_dataset=act_dataset.batch(1)
    for ii in range(350): #multiple different timesteps to get the averaged number of increased neuron activations from
        print("large batch ", lb_size, "iteration: ", ii)
        
        lb_batched_dataset=lb_batched_dataset.shuffle(shuffle_size)
        print("dataset_shuffled")

        if ii % 2 == 0:
            act_dataset=act_dataset.shuffle(shuffle_size)
            print("act_dataset_shuffled")
            zero_bias_vals = test_zero_biases(lb_model, act_dataset, activation_iters)
            lb_zb.append(zero_bias_vals)
            print("activations: ", zero_bias_vals)
            Make_summary_info({'zero_bias_vals':zero_bias_vals}, writer_lb, ii)

            sharp=sharpness(lb_model, lb_batched_dataset, sharp_iters, sb_size, num_batch_calc_loss)
            lb_s.append(sharp)
            print("sharpness: ", sharp)
            Make_summary_info({'sharpness':sharp}, writer_lb, ii)
        else:
            loss = fit_large_batch(lb_model, lb_size, sb_size, lb_batched_dataset, lr) 
            lb_l.append(loss)
            print("loss: ", loss)
            Make_summary_info({'loss':loss}, writer_lb, ii)

    #save each zero_bias_vals in an array for sb and lb then save each sharpness at the corresponding point into an array for sb and lb
    #then put into matlab file .mat
    from scipy.io import savemat
    savemat_name = 'activations_sharpness_1' # for simplicity of figuring the name
    savemat(savemat_name+'.mat',{'activations_sb':sb_zb,
                                 'activations_lb':lb_zb,
                                 'loss_sb':sb_l,
                                 'loss_lb':lb_l,
                                 'sharpness_sb':sb_s,
                                 'sharpness_lb':lb_s,}, oned_as='column')

    print("on testing data:")
    dataset=make_dataset(test_in, test_label, shuffle_size)
    dataset=dataset.batch(sb_size) #sb so it can fit
    sb_loss = full_loss(sb_model, dataset, sb_size)
    print("sb full loss: ", sb_loss)
    lb_loss = full_loss(sb_model, dataset, sb_size)
    print("lb full loss: ", lb_loss)


if __name__ == "__main__":
    main()