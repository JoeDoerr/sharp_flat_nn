#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Optimization pathway for testing small batch and large batch on MNIST dataset

@author: Joe Doerr
"""

import sys
sys.path.append("../")

#other file includes
from run import sb_fn, lb_fn, mc_fn

def main():
    sb, writer_sb = sb_fn(600, 100, 2, 30)
    lb, writer_lb = lb_fn(600, 6000, 100, 2, 30)
    sb_sharp, lb_sharp = mc_fn(sb, lb, writer_sb, writer_lb)
    print("sb_sharpness with 2 layers of 30: ", sb_sharp, "lb_sharpness with 2 layers of 30: ", lb_sharp)

    sb, writer_sb = sb_fn(600, 70, 3, 100)
    lb, writer_lb = lb_fn(600, 6000, 70, 3, 100)
    sb_sharp, lb_sharp = mc_fn(sb, lb, writer_sb, writer_lb)
    print("sb_sharpness with 3 layers of 100: ", sb_sharp, "lb_sharpness with 3 layers of 100: ", lb_sharp)

if __name__ == "__main__":
    main()