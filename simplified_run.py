#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Running functionalities very separately->
->Different objective functions
->calculate gradients
->applying gradients
->calculating loss over entire dataset
->calculating sharpness

@author: Joe Doerr
"""

import sys

sys.path.append("../")

import numpy as np
import tensorflow as tf
import os
currentdir = os.path.dirname(os.path.realpath(__file__)) #macro folder
parentdir = os.path.dirname(currentdir) #example folder
save_model_path_sb=currentdir+'/models/optimization_minimizers_sb'
save_model_path_lb=currentdir+'/models/optimization_minimizers_lb'

#other file includes
from prop_layers import nn_model
#from data_MNIST import train_in, train_label, test_in, test_label
from build import train_model, get_Loss, minimizer_value
from tflog import Make_summary_info

def cross_entropy(out, label, batch_size):
    #sum: -log(outputted_value) * true_value
    cross_entropy_loss = 0.0
    for i in range(batch_size):
      loss_vector = tf.math.log(out[i, :] + 0.001) #after training once, the outputs become 0's and a 1, so log makes them all inf
      loss_vector = tf.math.multiply(loss_vector, label[i, :])
      cross_entropy_loss += tf.reduce_sum(loss_vector)

    cross_entropy_loss *= -1
    cross_entropy_loss /= batch_size
    #print(cross_entropy_loss)
    return cross_entropy_loss

@tf.function
def calculate_gradients(model:nn_model, minibatch, batch_size, obj_function='CE'):
    obj_func_dict={'CE':cross_entropy} #only CE so far but can add more extremely easily
    with tf.GradientTape() as tape:
        input, label = minibatch
        out = model(input)
        loss = obj_func_dict[obj_function](out, label, batch_size) #always out, label, batch_size
        grads = tape.gradient(target=loss, sources=model.trainable_variables)
        return grads

@tf.function
def calculate_gradients_loss(model:nn_model, minibatch, batch_size, obj_function='CE'):
    obj_func_dict={'CE':cross_entropy} #only CE so far but can add more extremely easily
    with tf.GradientTape() as tape:
        input, label = minibatch
        out = model(input)
        loss = obj_func_dict[obj_function](out, label, batch_size) #always out, label, batch_size
        grads = tape.gradient(target=loss, sources=model.trainable_variables)
        return grads, loss

def apply_grads(model:nn_model, lr, grads):
    optimizer = tf.keras.optimizers.Adam(lr)
    optimizer.apply_gradients(zip(grads, model.trainable_variables))

def make_dataset(input, label, shuffle_size):
    train_dataset=tf.data.Dataset.from_tensor_slices((input,label)).shuffle(int(shuffle_size))
    return train_dataset

def sharpness(model:nn_model, batched_dataset, iters, batch_size, num_batch_calc_loss=5, obj_function='CE', eps=0.1):
    pre_noise_L = full_loss(model, batched_dataset, batch_size, num_batch_calc_loss, obj_function)
    N_layers=len(model.dense_layers) 
    layers_np_original=[None]*(N_layers+1) #+1 for the output layer
    for i in range(N_layers):
        layers_np_original[i]=model.dense_layers[i].w.numpy()
    layers_np_original[N_layers]=model.output_layer.w.numpy()

    #print("calc post_noise")
    av_sharp=0.0
    for its in range(iters):
        for i in range(N_layers):
            model.dense_layers[i].w.assign(layers_np_original[i]+np.random.uniform(-eps, eps,np.shape(layers_np_original[i])))
        model.output_layer.w.assign(layers_np_original[N_layers]+np.random.uniform(-eps, eps,np.shape(layers_np_original[N_layers])))
        post_noise_L = full_loss(model, batched_dataset, batch_size, num_batch_calc_loss, obj_function)
        av_sharp+=(tf.abs(pre_noise_L-post_noise_L)/(1+pre_noise_L)*100).numpy()

    #set model back:
    for i in range(N_layers):
        model.dense_layers[i].w.assign(layers_np_original[i])
    model.output_layer.w.assign(layers_np_original[N_layers])

    return av_sharp / float(iters)

#made so @tf.function can be separated from full loss
@tf.function
def loss_minibatch(model:nn_model, minibatch, batch_size, obj_function='CE'):
    obj_func_dict={'CE':cross_entropy} #only CE so far but can add more extremely easily
    input, label = minibatch
    out = model(input)
    return tf.reduce_mean(obj_func_dict[obj_function](out, label, batch_size)) #always out, label, batch_size

def full_loss(model:nn_model, batched_dataset, batch_size, end_early_iters=100, obj_function='CE'): #high end_early means full
    #dataset batching is causing all the speed problems
    full_loss = 0.0
    rs = 0.0
    for minibatch in batched_dataset:
        #print("full loss, minibatch: ", rs)
        full_loss += loss_minibatch(model, minibatch, batch_size, obj_function)
        rs += 1.0
        if rs >= end_early_iters:
            break
    return full_loss / rs

#each zero bias gradient value is a neuron that is activated during forward propagation, activated meaning at derReLU=0, derTanh=0, derSigmoid=0
#BATCH SIZE MUST BE 1 TO CHECK FOR EACH SAMPLE'S ACTIVATIONS, SO SEND BATCH SIZE 1 GRADIENTS HERE
@tf.function
def zero_biases(grads):
    #reminder that gradients are same size regardless of batch size
    return_zero_bias=0.0
    for gb in range(len(grads)): #grads is a list of tensors the size of the weight and bias matrices
        #for 2 hidden layers| input to 1st weights, first bias, 1st hidden to 2nd weights, bias, weights, bias, last layer to output weights, output bias

        #for any amount of layers, the odd numbers will work so put excessive number here to work up to 6 hidden layers
        if gb == 1 or gb == 3 or gb == 5:
            return_zero_bias+=(tf.cast(tf.size(grads[gb]), dtype=tf.float32) - tf.cast(tf.math.count_nonzero(grads[gb]), dtype=tf.float32))
            #print("size of grad:", tf.size(grads[gb]))
            #print("number of nonzero:", tf.cast(tf.math.count_nonzero(grads[gb]), dtype=tf.float32))
    return return_zero_bias
        

def shuffle_batch_dataset(dataset, size): #if we batch it again it will bug when iterating
    return dataset.shuffle(size)

#desired_bs must be divisible by fit_bs, iters very large means will run whole dataset
def fit_large_batch(model:nn_model, desired_bs, fit_bs, batched_dataset, lr, obj_function='CE', iters=10000):
    #dataset=dataset.shuffle(int(desired_bs*100.0)).batch(int(fit_bs))
    done=desired_bs/fit_bs
    grads=[]
    b_num=0
    its=0
    av_loss=0.0
    av_loss_its=0.0
    for minibatch in batched_dataset:
        if b_num == 0:
            grads, loss = calculate_gradients_loss(model, minibatch, fit_bs, obj_function)
        elif b_num > 0:
            grads_add, loss = calculate_gradients_loss(model, minibatch, fit_bs, obj_function)
            for sg in range(len(grads)): #for each tensor in the list, tf.add them together
                grads[sg] = tf.add(grads[sg], grads_add[sg])
        av_loss+=loss
        av_loss_its+=1.0
        b_num+=1
        if done <= b_num:
            apply_grads(model, lr, grads)
            b_num=0.0
            its+=1
            if iters <= its:
                break
    return av_loss / av_loss_its

def test_zero_biases(model:nn_model, single_batched_dataset, iters=100, obj_function='CE'):
    #dataset=dataset.batch(1) #BS MUST BE 1 TO TEST FOR ACTIVATIONS FOR EACH SAMPLE
    #it still just breaks at rebatching
    its=0
    activations=0
    for minibatch in single_batched_dataset:
        grads = calculate_gradients(model, minibatch, 1, obj_function)
        activations+=zero_biases(grads)
        its+=1
        if iters <= its:
            break
    return (float(activations) / float(its))

def train(model:nn_model, bs, dataset, lr, obj_function='CE', iters=10000):
    #dataset=dataset.shuffle(int(bs*100.0)).batch(int(bs))
    its=0
    for minibatch in dataset:
        grads = calculate_gradients(model, minibatch, bs, obj_function)
        apply_grads(model, lr, grads)
        its+=1
        if iters <= its:
            break

def train_loss(model:nn_model, bs, dataset, lr, obj_function='CE', iters=10000):
    #dataset=dataset.shuffle(int(bs*100.0)).batch(int(bs))
    its=0
    av_loss=0.0
    for minibatch in dataset:
        grads, loss = calculate_gradients_loss(model, minibatch, bs, obj_function)
        av_loss+=loss
        apply_grads(model, lr, grads)
        its+=1
        if iters <= its:
            break
    return av_loss / float(its)

def train_loss_batched(model:nn_model, bs, batched_dataset, lr, obj_function='CE', iters=10000):
    its=0
    av_loss=0.0
    for minibatch in batched_dataset:
        grads, loss = calculate_gradients_loss(model, minibatch, bs, obj_function)
        av_loss+=loss
        apply_grads(model, lr, grads)
        its+=1
        if iters <= its:
            break
    return av_loss / float(its)

#with these methods, recreate every operation seen in the opt path so it works in those opt paths using this much cleaner file