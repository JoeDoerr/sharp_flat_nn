set(0,'defaultAxesFontSize',14)

mat_file='activations_sharpness_1.mat';
load(mat_file)

%'activations_sb':sb_zb
%'activations_lb':lb_zb
%'loss_sb':sb_l
%'loss_lb':lb_l
%'sharpness_sb':sb_s
%'sharpness_lb':lb_s
% for activations vs activations
clf;
X_list = 0:1:174;
yyaxis left;
act = reshape(activations_sb, 175, 1);
plot(X_list,act,'linewidth',1.5);
set(gca,'fontsize',26)
ylabel('\fontsize{26}small batch activations')
ylim([0, 180])

yyaxis right;
act = reshape(activations_lb, 175, 1);
plot(X_list,act,'linewidth',1.5);
ylabel('\fontsize{26}large batch activations')
ylim([0, 180])

xlabel('\fontsize{26}timesteps')
xlim([0, 174])

title('\fontsize{26}Small Batch Activations Vs Large Batch Activations')
%for sb activations and sharpness overlayed
clf;
X_list = 0:1:174;
yyaxis left;
act = reshape(activations_sb, 175, 1);
plot(X_list,act,'linewidth',1.5);
set(gca,'fontsize',26)
ylabel('\fontsize{26}small batch activations')
ylim([0, 180])

yyaxis right;
act = reshape(sharpness_sb, 175, 1);
plot(X_list,act,'linewidth',1.5);
ylabel('\fontsize{26}small batch sharpness')
ylim([0, 100])

xlabel('\fontsize{26}timesteps')
xlim([0, 174])
title('\fontsize{26}Small Batch Activations and Sharpness')

%for lb activations and sharpness overlayed
clf;
X_list = 0:1:174;
yyaxis left;
act = reshape(activations_lb, 175, 1);
plot(X_list,act,'linewidth',1.5);
set(gca,'fontsize',26)
ylabel('\fontsize{26}large batch activations')
ylim([0, 180])

yyaxis right;
act = reshape(sharpness_lb, 175, 1);
plot(X_list,act,'linewidth',1.5);
ylabel('\fontsize{26}large batch sharpness')
ylim([0, 100])

xlabel('\fontsize{26}timesteps')
xlim([0, 174])
title('\fontsize{26}Large Batch Activations and Sharpness')
