set(0,'defaultAxesFontSize',14)

mat_file='activations_sharpness_1.mat';
load(mat_file)

%'activations_sb':sb_zb
%'activations_lb':lb_zb
%'loss_sb':sb_l
%'loss_lb':lb_l
%'sharpness_sb':sb_s
%'sharpness_lb':lb_s

% small batch and large batch activations on left axis
% small batch and large batch sharpness on right axis

X_list = 0:1:174;
figure;
sb_act = reshape(activations_sb, 175, 1);
lb_act = reshape(activations_lb, 175, 1);

plot(X_list,sb_act,'linewidth',1.5);
hold on;
plot(X_list,lb_act,'linewidth',1.5);
hold off;
xlim([0, 174])
ylim([0, 180])
set(gcf,'Position',[403   430   376   236]);

figure;
sb_sharp = reshape(sharpness_sb, 175, 1)/100;
lb_sharp = reshape(sharpness_lb, 175, 1)/100;

plot(X_list,sb_sharp,'linewidth',1.5);
hold on;
plot(X_list,lb_sharp,'linewidth',1.5);
hold off;
ylim([0, 1])
xlim([0, 174])
set(gcf,'Position',[403   430   376   236]);