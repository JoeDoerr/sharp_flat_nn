#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Save the gradient of the network to see the rate of change of each parameter
For visualization of how multitudes of samples can fit into network due to nonlinearities

@author: Joe Doerr
"""

import sys
sys.path.append("../")
import tensorflow as tf
from prop_layers import nn_model

#@tf.function
def gradient_mapping(network_model, minibatch_train):
    with tf.GradientTape() as tape:

        input, _ = minibatch_train
        out = network_model(input) #the gradients are just 0, the rate of change with respect to these values are somehow zero, must be a bug in how its coded
        #the rate of change of the target, "output of network", with respect to the network parameters is the importance of all the parameters to that output
        out = tf.reduce_sum(out, 1) # the out before this [batchsize, outputvectorsize]
        out *= -1
        grads = tape.gradient(target=out, sources=network_model.trainable_variables)
        #print(grads)
        
    return grads


#the importance of each parameter to the output in a linear network depends on the input, the neuron feeding into the parameter will affect the rate of change of output
#with respect to the parameter. The difference in parameter importance based on the network state made by the inputs is there but its just a lot less than nonlinear
#the amount of less that it is causes the linear network to be unable to fit as much. It is all just have low parameter importance for each input

# make image from grads for tensorboard
def image_grads(grads): #grads are not going to fit into each other nicely, just use the inside weights
    for ii in range(len(grads)):
        grads[ii] = tf.abs(grads[ii])
    g_image = grads[2]
    for gg in range(len(grads)): #for each tensor in the list, tf.concat them together
        #for 3 layers| input to 1st weights, first bias, weights, bias, weights, bias, last layer to output weights, output bias
        if gg == 4:
            g_image = tf.concat([g_image, grads[gg]], 0)

    #Its the hidden layer 1 to hidden layer 2 weights concatenated to hidden layer 2 to hidden layer 3 weights

    # put it tf.expand_dims at 2 and 3
    print(g_image.shape)
    g_image = tf.squeeze(g_image)
    print(g_image.shape)
    g_image = tf.expand_dims(g_image, -1)
    g_image = tf.expand_dims(g_image, 0)
    print(g_image.shape)

    print("g_image before ", g_image)
    print("reduce min: ", tf.reduce_min(g_image))
    print("reduce_max: ", tf.reduce_max(g_image))
    print("subtract g_image - reduce_min(g_image): ", tf.subtract(g_image, tf.reduce_min(g_image)))

    #setting all gradient values between 0 and 1
    g_image = tf.divide(tf.subtract(g_image, tf.reduce_min(g_image)), tf.subtract(tf.reduce_max(g_image), tf.reduce_min(g_image)))

    print("g_image ", g_image)
    return g_image