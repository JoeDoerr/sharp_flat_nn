#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Get data and format it from MNIST

@author: Joe Doerr
"""

import sys
sys.path.append("../")
import numpy as np
import tensorflow as tf
from tensorflow import keras
from keras.datasets import mnist
from keras.utils import np_utils

#%%The MNIST data is split between 60,000 28 x 28 pixel training images and 10,000 28 x 28 pixel images
(train_in, train_label), (test_in, test_label) = mnist.load_data()

#for the training
#the input is 60000 inputs which are made up of 28x28 pixels
#the label is 60000 classifications

#the testing is made up of 10000

print("train_in shape", train_in.shape)
print("train_label shape", train_label.shape)
print("test_in shape", test_in.shape)
print("test_label shape", test_label.shape)
print(train_in.shape[0])

#%%use a 784-dim input layer then 5 layers of 512 with ReLU, can batch normalize those layers
#10 outputs of softmax
#they try small batch with ADAM and large batch with ADAM
#they use cross entropy

#format the input of 28x28 to be a single vector of 784
train_in = train_in.reshape(60000, 784)
test_in = test_in.reshape(10000, 784)

train_in = train_in.astype('float32')
test_in = test_in.astype('float32')

train_in /= 255.0
test_in /= 255.0

nb_classes = 10 #number of unique digits

train_label = np_utils.to_categorical(train_label, nb_classes)
test_label = np_utils.to_categorical(test_label, nb_classes)

test_dataset=tf.data.Dataset.from_tensor_slices((test_in,test_label)).batch(10)
#test label is a 60000 x , tensor of output labels
#test in is a 60000 x 784 tensor of inputs, tuple of ([60000 x ,], [60000 x 784]) turns into 60000 tuples of (1, tensor[784])
#we see in examples a tuple of 3 different 2 x 1 values, along the first dimension and set into individual tuples, 2 tuples
#all the values in the first spots match up in a tuple and all the values in the second spots match up in a tuple and so forth, for 60000 its 60000 tuples