# README #

Tests inspired by https://arxiv.org/pdf/1609.04836.pdf

### Goal ###

* Want to recreate flat and sharp minimizer results with small batch training and large batch training respectively using different noise application algorithm
* using MNIST dataset

### Files ###

* build.py file-> @tf.function for train_model | get_loss | (get sharpness)evaluate_minimizer methods
* run.py file-> optimization procedure methods | customizable to a degree small batch and large batch entire training, and print results and save in tensorboard | RETURNS THE TRAINED MODEL
* optimization_pathway-> Variations of files numbered for different optimization pathways for different experiments
* prop_layers file to define model
* tflog.py file for tensorboard visualization
* slurm_job folder for slurm job implementation for potentially running a tf.mirroredstrategy on multiple gpus at once (to run type in command line: sbatch slurm_jobs/slurm_MNIST.sh)
