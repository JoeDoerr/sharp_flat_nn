#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Optimization pathway for testing small batch and large batch on MNIST dataset

@author: Joe Doerr
"""

import sys
sys.path.append("../")

#other file includes
from run import custom_train, test_loss_and_sharpness
from gradient_mapping import gradient_mapping, image_grads
from prop_layers import nn_model
from data_MNIST import train_in, train_label, test_in, test_label
import tensorflow as tf
from tflog import Make_summary_info, Make_summary_info_image

import os
currentdir = os.path.dirname(os.path.realpath(__file__)) #macro folder
parentdir = os.path.dirname(currentdir) #example folder


def main():
    #nn_model(m_layers, m_input_size, m_hidden_size, m_output_size)
    #small batch is 600
    #large batch is 6000
    training_iterations = 1

    #---SB---#
    sb_model = nn_model(3, 784, 100, 10)
    log_path=currentdir+'/'+'logs'+'/sb'
    writer_sb = tf.summary.create_file_writer(log_path)
    #-- before training run gradient mapping --#
    train_dataset=tf.data.Dataset.from_tensor_slices((train_in,train_label)).shuffle(60000).batch(1)
    minibatch = tf.constant(0)
    for mini in train_dataset:
        minibatch = mini
        break #random value
    sb_grads = gradient_mapping(sb_model, minibatch)
    sb_image = image_grads(sb_grads)
    Make_summary_info_image({'grad_mapping':sb_image}, writer_sb, 0)
    #custom_train(sb_model, 600, training_iterations, train_in, train_label, writer_sb)

    #---LB---#
    print("lb")
    #lb checking number of activations check each lb timestep
    lb_model = nn_model(3, 784, 100, 10)
    print(lb_model.trainable_variables)
    log_path=currentdir+'/'+'logs'+'/lb'
    writer_lb = tf.summary.create_file_writer(log_path)
    lb_grads = gradient_mapping(lb_model, minibatch)
    lb_image = image_grads(lb_grads)
    Make_summary_info_image({'grad_mapping':lb_image}, writer_lb, 0)
    #custom_train(lb_model, 6000, training_iterations, train_in, train_label, writer_lb)

    #log sharpness and loss and accuracy:
    #test_loss_and_sharpness(sb_model, lb_model, writer_sb, writer_lb, test_in, test_label)


    print("linear")

    linear_model = nn_model(3, 784, 100, 10, False)
    log_path=currentdir+'/'+'logs'+'/linear'
    writer_linear = tf.summary.create_file_writer(log_path)
    #custom_train(linear_model, 600, training_iterations, train_in, train_label, writer_linear)
    linear_grads = gradient_mapping(linear_model, minibatch)
    linear_image = image_grads(linear_grads)
    Make_summary_info_image({'grad_mapping':linear_image}, writer_linear, 0)
    return

    print("gradient mapping:")

    #-- run gradient mapping --#
    sb_grads = gradient_mapping(sb_model, minibatch)
    for ii in range(len(sb_grads)):
        print(ii, "small batch grads:", sb_grads[ii].shape)
    sb_image = image_grads(sb_grads)
    Make_summary_info_image({'grad_mapping':sb_image}, writer_sb, 1)

    lb_grads = gradient_mapping(lb_model, minibatch)
    for ii in range(len(lb_grads)):
        print(ii, "large batch grads:", lb_grads[ii].shape)
    print(lb_model.trainable_variables)
    lb_image = image_grads(lb_grads)
    Make_summary_info_image({'grad_mapping':lb_image}, writer_lb, 1)

    linear_grads = gradient_mapping(linear_model, minibatch)
    linear_image = image_grads(linear_grads)
    Make_summary_info_image({'grad_mapping':linear_image}, writer_linear, 0)

    #-- average gradient values --#
    Make_summary_info({'average gradient':tf.reduce_sum(tf.abs(tf.squeeze(sb_image)))}, writer_sb, 0)
    Make_summary_info({'average gradient':tf.reduce_sum(tf.abs(tf.squeeze(lb_image)))}, writer_lb, 0)
    Make_summary_info({'average gradient':tf.reduce_sum(tf.abs(tf.squeeze(linear_image)))}, writer_linear, 0)

if __name__ == "__main__":
    main()