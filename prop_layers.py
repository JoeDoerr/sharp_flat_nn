#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Propagation Layers and Model using Keras Subclassing

@author: Joe Doerr
"""

import sys
sys.path.append("../")
#import os
#currentdir = os.path.dirname(os.path.realpath(__file__)) #example folder
#parentdir = os.path.dirname(currentdir) #light conversion folder
#sys.path.append(parentdir)

import numpy as np
import tensorflow as tf
from tensorflow import keras
#from keras.datasets import mnist
#from keras.utils import np_utils

#%%make the model

#use keras subclassing and make a layer class with a weight variable matrix
#then a variable vector for bias
#then have the call function take in the vector of input, matrix multiply it to the weight
#then take that output and elementwise sum with the bias for the layer output
#make the model class and when building it choose how many of the layer classes it wants
#then have that call function just call each layer in order
#use gradient tape for updating to find the gradient then apply it
#things like dropout and batchnorm would have to be manually made

class dense_layer(keras.layers.Layer):
  def __init__(self, prev_n, n, relu=True, softmax=False, batch_norm=True):
    super(dense_layer, self).__init__()

    w_init = tf.random_normal_initializer()
    #input is 784, so [784] * [784][512] means that it will output a [512]
    self.w = tf.Variable(initial_value=w_init(shape=(prev_n, n), dtype="float32"), trainable=True, name="dense_layer")

    self.b = tf.Variable(initial_value=w_init(shape=(n,), dtype="float32"), trainable=True, name="dense_bias")

    self.relu = relu
    self.softmax = softmax

  def call(self, input):
    x = tf.matmul(input, self.w)
    x = x + self.b
    
    if self.relu == True:
      x = tf.nn.relu(x)
    elif self.softmax == True:
      x = tf.map_fn(tf.nn.softmax, x)
      for each in x:
        tf.nn.softmax(each)
  
    #if self.batch_norm == True:
      #adwaddadwa
    return x

class nn_model(keras.Model):
  def __init__(self, numLayers, input_size, layer_size, output_size, relu = True, softmax = False, tanh = False):
    super(nn_model, self).__init__()

    #make input, middle, and output layers
    self.dense_layers = []
    self.dense_layers.append(dense_layer(input_size, layer_size, relu=relu))

    for ii in range(numLayers-1):
      self.dense_layers.append(dense_layer(layer_size, layer_size, relu=relu))

    self.output_layer = (dense_layer(layer_size, output_size, relu=False, softmax=True))
  
  def call(self, input):
    x = input
    for ii in range(len(self.dense_layers)):
      x = self.dense_layers[ii](x)
    return self.output_layer(x)