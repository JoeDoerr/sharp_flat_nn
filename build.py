#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Methods to run optimization and minimizer tests

@author: Joe Doerr
"""

import sys
sys.path.append("../")
import numpy as np
import tensorflow as tf
from tensorflow import keras
from keras.datasets import mnist
from keras.utils import np_utils

#%%training and testing loss and adding noise to test minimizer
#get the minibatch_train, for each batch element, get the loss, average the losses, then compute and return the gradients
@tf.function
def train_model(network_model, minibatch_train, batch_size, out_loss = False):
  with tf.GradientTape() as tape:
    input, label = minibatch_train
    out = network_model(input)
    #outputs (batch_size, 10)

    #cross entropy
    #-1/N * sum(label * log(out)) One thing to note about this is I am giving N the value of the current batch size coming into the train_model function
    cross_entropy_loss = 0.0
    #the output is [batch_size][10]
    #the label is [batch_size]
    for i in range(batch_size):
      #after training once, the outputs become 0's and a 1, so log makes them all inf
      #print("before log", out[i, :] + 0.01)
      loss_vector = tf.math.log(out[i, :] + 0.01) #add 0.01 because the outputs are all 0's and a 1, and log(0) = inf, if truth value wants increase -log(0.01) will be 2 loss, so positive rate of change for the variable
      #matrix multiply vectors so that only the label element isn't 0
      loss_vector = tf.math.multiply(loss_vector, label[i, :]) #which batch to take the [10] from

      cross_entropy_loss += tf.reduce_sum(loss_vector) #sum up all the log(p) inside the loss_vector

    #then make the whole thing negative at the end
    cross_entropy_loss *= -1 #get the loss then want to reduce it, so move that 0 towards 1 by making the high positive number -1 * (-2.0) = 2 go towards 0
    cross_entropy_loss /= batch_size
    print(cross_entropy_loss)

    grads = tape.gradient(target=cross_entropy_loss, sources=network_model.trainable_variables)
    #print(grads)
    #optimizer.apply_gradients(zip(grads, network_model.trainable_variables))
    if out_loss == True:
      return cross_entropy_loss, grads
    return grads #cuts it off neet to change this
    
#goes through the whole dataset and averages all the losses together
def get_Loss(network_model, dataset, batch_size):
  av_loss = 0.0
  divide_loss = 0.0
  for minibatch_train in dataset:
    input, label = minibatch_train
    out = network_model(input)
    
    cross_entropy_loss = 0.0
    for i in range(batch_size):

      loss_vector = tf.math.log(out[i, :] + 0.01)

      loss_vector = tf.math.multiply(loss_vector, label[i, :])

      cross_entropy_loss += tf.reduce_sum(loss_vector)

    cross_entropy_loss *= -1
    cross_entropy_loss /= batch_size
    #print("partial cross entropy loss", cross_entropy_loss)
    av_loss += cross_entropy_loss
    divide_loss += 1.0
    
  av_loss /= divide_loss
  #print("real cross entropy loss on test data", av_loss)
  return av_loss

#test sharpness
from prop_layers import nn_model
def minimizer_value(model:nn_model, dataset, batch_size, eps=0.01):
    pre_noise_L = get_Loss(model, dataset, batch_size)
    for i in range(len(model.dense_layers)):
        model.dense_layers[i].w=model.dense_layers[i].w+tf.random.uniform(tf.shape(model.dense_layers[i].w), minval=-eps, maxval=eps)
    model.output_layer.w=model.output_layer.w+tf.random.uniform(tf.shape(model.output_layer.w), minval=-eps, maxval=eps)
    post_noise_L = get_Loss(model, dataset, batch_size)
    return ((post_noise_L - pre_noise_L) / (1 + pre_noise_L)) * 100