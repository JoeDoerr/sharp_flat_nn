#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Uses build.py methods to create complete optimization and sharpness functions

@author: Joe Doerr
"""

import sys

sys.path.append("../")

#import numpy as np
import tensorflow as tf
#from tensorflow import keras
#from keras.datasets import mnist
#from keras.utils import np_utils

import os
currentdir = os.path.dirname(os.path.realpath(__file__)) #macro folder
parentdir = os.path.dirname(currentdir) #example folder
save_model_path_sb=currentdir+'/models/optimization_minimizers_sb'
save_model_path_lb=currentdir+'/models/optimization_minimizers_lb'


#other file includes
from prop_layers import nn_model
#from data_MNIST import train_in, train_label, test_in, test_label
from build import train_model, get_Loss, minimizer_value
from tflog import Make_summary_info


#optimizer = tf.keras.optimizers.Adam(0.0005)
learning_rate=tf.Variable(0.0003,trainable=False,name='learning_rate')
optimizer = tf.keras.optimizers.Adam(learning_rate)

def test_loss_and_sharpness(sb, lb, writer_sb, writer_lb, test_in, test_label):
  batch_size = 500 #testing batch_size to fit in with test data
  test_dataset=tf.data.Dataset.from_tensor_slices((test_in,test_label)).shuffle(batch_size).batch(batch_size)

  # sb
  av_loss = get_Loss(sb, test_dataset, batch_size)
  acc = 1.0 - av_loss
  print("small batch average loss", av_loss)
  print("small batch average accuracy", acc)
  Make_summary_info({'av_loss':av_loss, 'acc':acc}, writer_sb, 0)
  
  # lb
  av_loss = get_Loss(lb, test_dataset, batch_size)
  acc = 1.0 - av_loss
  print("large batch average loss", av_loss)
  print("large batch average accuracy", acc)
  Make_summary_info({'av_loss':av_loss, 'acc':acc}, writer_lb, 0)

  #both minimizer calc
  minimizer_calculation(sb, lb, writer_sb, writer_lb, test_in, test_label)

#get number of zero bias vals in the gradient of each sample in the whole training set
def number_of_activations_train(model:nn_model, batch_size:int, train_iters:int, train_in, train_label):
  """
  We do one at a time as batchsize=1 so that we can look at gradients for each single input/label pair and save the number of zero rows
  """
  real_batch_size = batch_size
  batch_size = 1
  return_zero_bias = 0
  each_update_bias = 0
  train_dataset=tf.data.Dataset.from_tensor_slices((train_in,train_label)).shuffle(60000).batch(batch_size)
  for ii in range(train_iters): #for each full time we want to run this
    sum_batch_num = 0
    grads = []
    print("train_iter: ", ii)
    for minibatch in train_dataset: #I do scale_to_fit(10) minibatches as one batch
      temp_gradient = train_model(model, minibatch, batch_size)
      if sum_batch_num == 0:
        grads = temp_gradient
      elif sum_batch_num > 0:
        grads_add = temp_gradient
        for sg in range(len(grads)): #for each tensor in the list, tf.add them together
          grads[sg] = tf.add(grads[sg], grads_add[sg])

      #get the number of activations by finding the number of 0 bias val in the gradient and add it to the zero_bias as a float because we later divide
      # each gradient is a list of tensors, with each value in the list a different set of weights
      for gb in range(len(grads)): #after the first two, every other tensor
        #for 3 layers| input to 1st weights, first bias, weights, bias, weights, bias, last layer to output weights, output bias
        #want iterations 1, 3, 5
        if gb == 1 or gb == 3 or gb == 5:
          return_zero_bias += (100 - tf.math.count_nonzero(temp_gradient[gb]))
          each_update_bias += (100 - tf.math.count_nonzero(temp_gradient[gb]))
      # lower number of zero bias values means there are more unactivated values

      sum_batch_num += 1

      if sum_batch_num == real_batch_size:
        sum_batch_num = 0
        divide_by = tf.constant([float(real_batch_size)])
        for sg in range(len(grads)): #for each tensor in the list, tf.divide them by the number of grads added together
          grads[sg] = tf.divide(grads[sg], divide_by)

        print(float(each_update_bias) / float(real_batch_size))
        each_update_bias = 0

        optimizer.apply_gradients(zip(grads, model.trainable_variables))
        grads = []
  """
  training complete, return number of zero bias vals
  """
  return float(return_zero_bias) / 60000

#pass in the model
def custom_train(model:nn_model, batch_size:int, train_iters:int, train_in, train_label, writer, starting_iterator:int = 0):
  """
  We divide batch size by 10 so that we can put large batch size in and still can fit on the gpu
  We run the batch sizes from the whole train_dataset and sum them up until 10 and apply each time
  Can run sb and lb with this one function
  """
  scale_to_fit = 10
  batch_size = int(batch_size / scale_to_fit)
  train_dataset=tf.data.Dataset.from_tensor_slices((train_in,train_label)).shuffle(60000).batch(batch_size)
  for ii in range(starting_iterator, train_iters): #for each full time we want to run this
    summed_loss = 0.0
    sum_batch_num = 0
    grads = []
    
    print("train_iter: ", ii)
    for minibatch in train_dataset: #I do scale_to_fit(10) minibatches as one batch
      if sum_batch_num == 0:
        loss, grads = train_model(model, minibatch, batch_size, True)
      elif sum_batch_num > 0:
        loss, grads_add = train_model(model, minibatch, batch_size, True)
        for sg in range(len(grads)): #for each tensor in the list, tf.add them together
          grads[sg] = tf.add(grads[sg], grads_add[sg])

      summed_loss += loss
      sum_batch_num += 1

      if sum_batch_num == scale_to_fit:
        sum_batch_num = 0
        divide_by = tf.constant([float(scale_to_fit)])
        for sg in range(len(grads)): #for each tensor in the list, tf.divide them by the number of grads added together
          grads[sg] = tf.divide(grads[sg], divide_by)
        optimizer.apply_gradients(zip(grads, model.trainable_variables))
        grads = []
    #loss is averaged for each batch, so average all averaged batches summed by number of batches
    Make_summary_info({'loss':summed_loss/(float(train_in.shape[0])/10.0)}, writer, ii)
  """
  training complete, return model and loss
  """
  return model

#dep
def optimize_sb(small_batch_size:int, train_iters:int, m_layers:int, m_input_size:int, m_hidden_size:int, m_output_size:int, train_in, train_label, test_in, test_label):
  """
  for small batch, for a number of iterations we feed the whole dataset through
  each batch of 500 we send through, calculate gradients, then update the model
  """
  batch_size = small_batch_size #sb
  train_dataset=tf.data.Dataset.from_tensor_slices((train_in,train_label)).shuffle(batch_size).batch(batch_size)
  #makes individual tuples of (in, out) by slicing off guys from the 1st axis (batch)
  #then batches these tuples after shuffling using the .batch(size)
  #this results in a tuple of (tensor(batch_size, input=784), tensor(batch_size, label=1))
  sb = nn_model(m_layers, m_input_size, m_hidden_size, m_output_size)

  for ii in range(train_iters):
    print("sb_train_iter: ", ii)
    for minibatch in train_dataset:
      grads = train_model(sb, minibatch, batch_size)
      optimizer.apply_gradients(zip(grads, sb.trainable_variables))

  """
  training complete, now to test on the testing data
  """
  batch_size = 500 #testing batch_size to fit in with test data
  test_dataset=tf.data.Dataset.from_tensor_slices((test_in,test_label)).shuffle(batch_size).batch(batch_size)
  av_loss = get_Loss(sb, test_dataset, batch_size)
  acc = 1.0 - av_loss
  print("small batch average loss", av_loss)
  print("small batch average accuracy", acc)

  log_path=parentdir+'/'+'logs'+'/sb'+'_layers_%s_hidden_size_%s' %(m_layers, m_hidden_size)
  writer_sb = tf.summary.create_file_writer(log_path)
  Make_summary_info({'av_loss':av_loss, 'acc':acc}, writer_sb, 0)
  return sb, writer_sb

#dep
def optimize_lb(temp_batch_size:int, large_batch_size:int, train_iters:int, m_layers:int, m_input_size:int, m_hidden_size:int, m_output_size:int, train_in, train_label, test_in, test_label):
  """
  for large batch, for a number of iterations we feed the whole dataset through
  instead of updating on each batch of 500 (same as sb) that we send through, we wait and continue to average them
  after 10 of the 500 batches, we have essentially a 5000 batch gradient
  we then apply those gradients then continue
  """

  learning_rate.assign(0.0005125)
  batch_size = temp_batch_size #lb
  train_dataset=tf.data.Dataset.from_tensor_slices((train_in,train_label)).shuffle(batch_size).batch(batch_size)

  lb = nn_model(m_layers, m_input_size, m_hidden_size, m_output_size)
  for ii in range(train_iters): #for each full time we want to run this
    loops = 0
    grads = []
    print("lb_train_iter: ", ii)
    for minibatch in train_dataset: #for each minibatch, I do 10 counting with loops, once 10 are done I apply the averaged gradients

      #starting the grads or adding together
      if loops == 0:
        grads = train_model(lb, minibatch, batch_size)
      elif loops > 0:
        grads_add = train_model(lb, minibatch, batch_size)
        for sg in range(len(grads)): #for each tensor in the list, tf.add them together
          grads[sg] = tf.add(grads[sg], grads_add[sg])

      loops += 1

      if loops >= (float(large_batch_size) / float(temp_batch_size)): #once I do enough minibatches to fill up the large batch size, apply the averaged gradients as a larger batch update
        loops = 0
        divide_by = tf.constant([10.0])
        for sg in range(len(grads)): #for each tensor in the list, tf.divide them by the number of grads added together
          grads[sg] = tf.divide(grads[sg], divide_by)
        optimizer.apply_gradients(zip(grads, lb.trainable_variables))
        grads = []

  """
  training complete, now to test on the testing data
  """
  batch_size = 500 #testing batch_size to fit in with test data, should fix this in get_loss so it can be any batch_size and now throw an error
  test_dataset=tf.data.Dataset.from_tensor_slices((test_in,test_label)).shuffle(batch_size).batch(batch_size)
  av_loss = get_Loss(lb, test_dataset, batch_size)
  acc = 1.0 - av_loss
  print("large batch average loss", av_loss)
  print("large average accuracy", acc)

  log_path=parentdir+'/'+'logs'+'/lb'+'_layers_%s_hidden_size_%s' %(m_layers, m_hidden_size)
  writer_lb = tf.summary.create_file_writer(log_path)
  Make_summary_info({'av_loss':av_loss, 'acc':acc}, writer_lb, 0)
  return lb, writer_lb

@tf.function
def minimizer_calculation(sb, lb, writer_sb, writer_lb, test_in, test_label):
  #testing sb and lb for minimizer
  #first save the models so that we can revert them
  """
  testing sharpness of the sb vs lb models, sharpness being the highest value this metrix hits over multiple tests -> predicted that lb should have higher sharpness (worse)
  """
  sb.save_weights(save_model_path_sb)
  lb.save_weights(save_model_path_lb)

  batch_size = 500 #testing batch_size just to go faster
  test_dataset=tf.data.Dataset.from_tensor_slices((test_in,test_label)).shuffle(batch_size).batch(batch_size)

  sb_top_m = 0.0
  lb_top_m = 0.0
  sb_av_m =  0.0
  lb_av_m =  0.0
  m_sample_num = 10.0

  for jj in range(m_sample_num):
      sb.load_weights(save_model_path_sb)
      lb.load_weights(save_model_path_lb)

      sb_m = minimizer_value(sb, test_dataset, batch_size) #worst sb minimizer
      if abs(sb_top_m) < abs(sb_m):
          sb_top_m = sb_m
      sb_av_m += sb_m
      Make_summary_info({'sharpness':sb_m}, writer_sb, jj)

      lb_m = minimizer_value(lb, test_dataset, batch_size) #worst lb minimizer
      if abs(lb_top_m) < abs(lb_m):
          lb_top_m = lb_m
      lb_av_m += lb_m
      Make_summary_info({'sharpness':lb_m}, writer_lb, jj)

  #print("small batch worst sharpness", sb_top_m)
  #print("large batch worst sharpness", lb_top_m)
  Make_summary_info({'worst_sharpness':sb_top_m}, writer_sb, 0)
  Make_summary_info({'worst_sharpness':lb_top_m}, writer_lb, 0)
  return sb_top_m, lb_top_m, sb_av_m/m_sample_num, lb_av_m/m_sample_num

@tf.function
def net_minimizer_calc(net, writer_net, test_in, test_label):
  #testing sb and lb for minimizer
  #first save the models so that we can revert them
  """
  testing sharpness of the sb vs lb models, sharpness being the highest value this metrix hits over multiple tests -> predicted that lb should have higher sharpness (worse)
  """
  net.save_weights(save_model_path_sb)
  lb.save_weights(save_model_path_lb)

  batch_size = 500 #testing batch_size just to go faster
  test_dataset=tf.data.Dataset.from_tensor_slices((test_in,test_label)).shuffle(batch_size).batch(batch_size)

  sb_top_m = 0.0
  lb_top_m = 0.0
  sb_av_m =  0.0
  lb_av_m =  0.0
  m_sample_num = 10.0

  for jj in range(m_sample_num):
      sb.load_weights(save_model_path_sb)
      lb.load_weights(save_model_path_lb)

      sb_m = minimizer_value(sb, test_dataset, batch_size) #worst sb minimizer
      if abs(sb_top_m) < abs(sb_m):
          sb_top_m = sb_m
      sb_av_m += sb_m
      Make_summary_info({'sharpness':sb_m}, writer_sb, jj)

      lb_m = minimizer_value(lb, test_dataset, batch_size) #worst lb minimizer
      if abs(lb_top_m) < abs(lb_m):
          lb_top_m = lb_m
      lb_av_m += lb_m
      Make_summary_info({'sharpness':lb_m}, writer_lb, jj)

  #print("small batch worst sharpness", sb_top_m)
  #print("large batch worst sharpness", lb_top_m)
  Make_summary_info({'worst_sharpness':sb_top_m}, writer_sb, 0)
  Make_summary_info({'worst_sharpness':lb_top_m}, writer_lb, 0)
  return sb_top_m, lb_top_m, sb_av_m/m_sample_num, lb_av_m/m_sample_num


#for ease of MNIST testing
from data_MNIST import train_in, train_label, test_in, test_label
#optimize_sb(small_batch_size:int, train_iters:int, m_layers:int, m_input_size:int, m_hidden_size:int, m_output_size:int, train_in, train_label, test_in, test_label):
#optimize_lb(temp_batch_size:int, large_batch_size:int, train_iters:int, m_layers:int, m_input_size:int, m_hidden_size:int, m_output_size:int, train_in, train_label, test_in, test_label):
sb_fn=lambda batch_size, train_it, m_layers, m_hidden: optimize_sb(batch_size, train_it, m_layers, 784, m_hidden, 10, train_in, train_label, test_in, test_label)
lb_fn=lambda batch_size, lbs, train_it, m_layers, m_hidden: optimize_lb(batch_size, lbs, train_it, m_layers, 784, m_hidden, 10, train_in, train_label, test_in, test_label)
mc_fn=lambda sb, lb, writer_sb, writer_lb: minimizer_calculation(sb, lb, writer_sb, writer_lb, 500, test_in, test_label)